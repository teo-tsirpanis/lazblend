{ LazBlend main unit

  Copyright (C) 2015 Theodore Tsirpanis (teo.tisrpanis.718@gmail.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
  MA 02111-1307, USA.
}
unit RegisterLazBlend;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, SrcEditorIntf, MenuIntf, IDECommands, LCLType;

resourcestring
  rsDisable = 'Disable';
  rsDisablesSourceEditorTranslucency = 'Disables source editor translucency';
  rsLazBlendOptions = 'LazBlend options';
  rsSetsSourceEditorTranslucencyTo83 = 'Sets source editor translucency to 83%';
  rsSetsSourceEditorTranslucencyTo86 = 'Sets source editor translucency to 86%';
  rsSetsSourceEditorTranslucencyTo89 = 'Sets source editor translucency to 89%';
  rsSetsSourceEditorTranslucencyTo92 = 'Sets source editor translucency to 92%';
  rsSetsSourceEditorTranslucencyTo95 = 'Sets source editor translucency to 95%';

procedure Register;

implementation

procedure DoSetOpacity(const aOpacity: byte);
var
  vAlphaBlend: boolean;
  i: integer;
begin
  if aOpacity = 0 then
    Exit;
  vAlphaBlend := aOpacity <> 255;
  for i := 0 to SourceEditorManagerIntf.SourceWindowCount - 1 do
    with SourceEditorManagerIntf.SourceWindows[i] do
    begin
      AlphaBlend := vAlphaBlend;
      AlphaBlendValue := aOpacity;
    end;
end;

procedure DoDisable(Sender: TObject);
begin
  DoSetOpacity(255);
end;

procedure DoSet95(Sender: TObject);
begin
  DoSetOpacity(243);
end;

procedure DoSet92(Sender: TObject);
begin
  DoSetOpacity(236);
end;

procedure DoSet89(Sender: TObject);
begin
  DoSetOpacity(228);
end;

procedure DoSet86(Sender: TObject);
begin
  DoSetOpacity(220);
end;

procedure DoSet83(Sender: TObject);
begin
  DoSetOpacity(212);
end;

procedure Register;
var
  Cat: TIDECommandCategory;
  aSubMenu: TIDEMenuSection;
  aCommands: array[0..5] of TIDECommand;
begin
  Cat := IDECommandList.CreateCategory(nil, 'LazBlend', rsLazBlendOptions);
  aSubMenu := RegisterIDESubMenu(itmSecondaryTools, 'lazblend_root', 'LazBlend');
  aCommands[0] := RegisterIDECommand(Cat, 'lazblend_255',
    rsDisablesSourceEditorTranslucency, IDEShortCut(VK_F7, [ssCtrl, ssShift]),
    nil, @DoDisable);
  aCommands[1] := RegisterIDECommand(Cat, 'lazblend_243',
    rsSetsSourceEditorTranslucencyTo95, IDEShortCut(VK_F2, [ssCtrl, ssShift]),
    nil, @DoSet95);
  aCommands[2] := RegisterIDECommand(Cat, 'lazblend_236',
    rsSetsSourceEditorTranslucencyTo92, IDEShortCut(VK_F3, [ssCtrl, ssShift]),
    nil, @DoSet92);
  aCommands[3] := RegisterIDECommand(Cat, 'lazblend_228',
    rsSetsSourceEditorTranslucencyTo89, IDEShortCut(VK_F4, [ssCtrl, ssShift]),
    nil, @DoSet89);
  aCommands[4] := RegisterIDECommand(Cat, 'lazblend_220',
    rsSetsSourceEditorTranslucencyTo86, IDEShortCut(VK_F5, [ssCtrl, ssShift]),
    nil, @DoSet86);
  aCommands[5] := RegisterIDECommand(Cat, 'lazblend_212',
    rsSetsSourceEditorTranslucencyTo83, IDEShortCut(VK_F6, [ssCtrl, ssShift]),
    nil, @DoSet83);
  RegisterIDEMenuCommand(aSubMenu, 'lazblend_menu_95', '95%', nil, nil, aCommands[1]);
  RegisterIDEMenuCommand(aSubMenu, 'lazblend_menu_92', '92%', nil, nil, aCommands[2]);
  RegisterIDEMenuCommand(aSubMenu, 'lazblend_menu_89', '89%', nil, nil, aCommands[3]);
  RegisterIDEMenuCommand(aSubMenu, 'lazblend_menu_86', '86%', nil, nil, aCommands[4]);
  RegisterIDEMenuCommand(aSubMenu, 'lazblend_menu_83', '83%', nil, nil, aCommands[5]);
  RegisterIDEMenuCommand(aSubMenu, '', '-');
  RegisterIDEMenuCommand(aSubMenu, 'lazblend_menu_100', rsDisable,
    nil, nil, aCommands[0]);
end;

end.
